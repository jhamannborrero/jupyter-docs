import os
from distutils.util import strtobool

bind = "0.0.0.0:" + os.getenv("FLASK_RUN_PORT", "5000")
accesslog = "-"
access_log_format = "%(h)s %(l)s %(u)s %(t)s '%(r)s' %(s)s %(b)s '%(f)s' '%(a)s' in %(L)ss"
errorlog = "-"
loglevel = os.getenv("GUNICORN_LOG_LEVEL", "debug")
worker_class = os.getenv("GUNICORN_WORKER_CLASS", "gthread")
reload = bool(strtobool(os.getenv("GUNICORN_RELOAD", "true")))
timeout = 60
workers = 5
threads = int(os.getenv("GUNICORN_THREADS", 8))