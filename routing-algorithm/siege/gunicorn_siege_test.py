import random as rn
import pandas as pd
import sys
sys.path.append('/home/jehamann/eClever/repositories/route-calculation/')

from subprocess import Popen, PIPE
import subprocess
import multiprocessing
import os
#import time

from distutils.util import strtobool

cities = {'Berlin':(13.41053,52.52437),
          'Dresden':(13.731751, 51.056044),
          'Hamburg':(10.01534, 53.57532),
          'Muenchen':(11.576124,48.137154),
          'Koeln':(6.953101,50.935173),
          }

#plug configurations for different EVs
autos = {'Model S 75D':452,
         'Model 3':437,
         'Ioniq Elektro':306,
         'Leaf 40kWh':354,
         'e-Golf':516,
         'Model S 60':441,
         'ID.3':542
         }

def create_url_list():
    for ev in autos:
        for origin in cities:
            for destination in cities:
                if origin != destination:
                    with open('test_urls.txt', 'a') as f:
                        f.write("127.0.0.1:5000/v1/route-calculation?destinationLocationLat={}"\
                                "&destinationLocationLng={}&plugConfigurationId={}&socStart={}"\
                                "&startLocationLat={}&startLocationLng={}\n".format(
                                        cities[destination][1],
                                        cities[destination][0],
                                        autos[ev],
                                        rn.randrange(5,100),
                                        cities[origin][1],
                                        cities[origin][0],
                                        ))

def set_gunicorn_file(workers, threads):
    head = "import os\n"\
    "from distutils.util import strtobool\n\n"\
    'bind = "0.0.0.0:" + os.getenv("FLASK_RUN_PORT", "5000")\n'\
    'accesslog = "-"\n'\
    "access_log_format = \"%(h)s %(l)s %(u)s %(t)s '%(r)s' %(s)s %(b)s '%(f)s' '%(a)s' in %(L)ss\"\n"\
    'errorlog = "-"\n'\
    'loglevel = os.getenv("GUNICORN_LOG_LEVEL", "debug")\n'\
    'worker_class = os.getenv("GUNICORN_WORKER_CLASS", "gthread")\n'\
    'reload = bool(strtobool(os.getenv("GUNICORN_RELOAD", "true")))\n'\
    'timeout = 60\n'\
    "workers = {}\n"\
    'threads = int(os.getenv("GUNICORN_THREADS", {}))'.format(workers, threads)

    with open('gunicorn_test.py', 'w') as f:
        f.write(head)

def set_siege(workers, threads, concurrent, repetitions):
    return 'siege --concurrent={2} --reps={3} --header "Origin: localhost" -f '\
    'test_urls.txt --internet > test_{0}w_{1}th_{2}c_{3}r.txt'.format(
            workers, threads,int(concurrent), int(repetitions))

def set_siege_rampup(workers, threads, concurrent, session_time):
    return 'siege --concurrent={2} --time={3}M -d1 --header "Origin: localhost" -f '\
    'test_urls.txt --internet > test_rampup_{0}w_{1}th_{2}c.txt'.format(
            workers, threads,int(concurrent), int(session_time))

def set_gunicorn():
    return '/home/jehamann/eClever/repositories/tests/siege/venv/bin/gunicorn '\
            '--chdir /home/jehamann/eClever/repositories/route-calculation/src/ '\
            '-c gunicorn_test.py main:app &'


def read_session_res(file_name):
    with open(file_name, 'r') as f:
        timestamp =[]
        resp = []
        time = []
        for line in f:
            entry = line.split()
            timestamp.append('{} {}'.format(entry[1], entry[2][:-1]))
            resp.append(int(entry[4]))
            time.append(float(entry[5]))

    return pd.DataFrame({'timestamp':timestamp, 'response':resp, 'time':time})

def read_logs():
    file = '/home/jehamann/.siege/siege.log'

    return pd.read_csv(file)

def run(cmd, communicate=True):
    print('-'*40)
    print('running:', cmd)
    p = Popen(cmd, stderr=PIPE, stdout=PIPE, shell=True, close_fds=True)
    if communicate:
        output, errors = p.communicate()
#    print([p.returncode, errors, output])
        print('#'*10+' Output '+'#'*10+'\n', output, '\n','#'*10+' Errors '+'#'*10, '\n', errors)
        if p.returncode:# or errors:
            print('something went wrong...')
    return p

#params = [(100.0, 10), (50.0, 20), (25.0, 40), (20.0, 50), (10.0, 100), (5.0, 200), (4.0, 250), 
#          (2.0, 500), (1.0, 1000)]

#params = [(10.0, 100), (5.0, 200), (2.0, 500), (1.0, 1000)]
#params = [(1, (1+n)*50) for n in range(2)]
    

params = [(15.0, 20), (5.0, 60), (3.0, 100), (1.0, 300)]
cores = [n*2+1 for n in range(0,9,2)]

#for worker in cores:
#    threads = 8
#    set_gunicorn_file(worker, threads)
#    os.system(set_gunicorn())
#    time.sleep(10)
#    for reps, concur in params:
#        os.system(set_siege(worker, threads, concur, reps))
#
#    os.system('killall -r -9 gunicorn')


#for reps, concur in params[3:]:
#    worker = 5
#    threads = 1
#    os.system(set_siege(worker, threads, concur, reps))

#os.system('killall -r -9 gunicorn')


def ramp_up_test(worker, threads, session_time):
    for concur in [10*(n+1) for n in range(15)]:
#        print(set_siege_rampup(worker, threads, concur, session_time))
        os.system(set_siege_rampup(worker, threads, concur, session_time))

#ramp_up_test(1, 8, 10)
