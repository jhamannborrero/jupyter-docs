import pprint
import pandas as pd
import sys
sys.path.append('/home/jehamann/eClever/repositories/route-calculation/src/')

from services import RouteService
from models import  RouteRequest
from utils import RouteUtil

def print_solved_route_problem(route_data, solution):
        """...

        Arguments:
            routeData : ...
            solution : ...

        Returns:
            ...
        """

        # Print solution
        width = 12
        
        active_ids = [stop['stationId'] for stop in solution['stops']]
        active_nodes = [0]+[node for node in route_data['nodes'] if route_data['nodes'][node]['id'] in active_ids]+[len(route_data['nodes'])-1]
        active_arcs = [(active_nodes[i], active_nodes[i+1]) for i in range(len(active_nodes)-1)]

        # print arc info
        print('{:=^{width}}'.format('', width=5*width))
        print('{:{width}}{:>{width}}{:>{width}}{:>{width}}{:>{width}}'.format(
            'arc', 'Energy', 'Time', 'Distance', 'avg Speed', width=width))
        print('{:{width}}{:>{width}}{:>{width}}{:>{width}}{:>{width}}'.format(
            '', '[kWh]', '[min]', '[km]', '[km/h]', width=width))
        print('{:-^{width}}'.format('', width=5*width))

        for arc in active_arcs:
            print(
                '{:{width}}{:{width}}{:{width}.1f}{:{width}.1f}{:{width}.1f}'.format(
                    str(arc),
                    route_data['arcs'][arc]['consumption'],
                    route_data['arcs'][arc]['time'] / 60, route_data['arcs'][arc]['distance'] / 1000,
                    (route_data['arcs'][arc]['distance'] / 1000) / (route_data['arcs'][arc]['time'] / 3600),
                    width=width))

        print('{:=^{width}}'.format('', width=5*width))

        # Print charging stops info
        print('{:=^{width}}'.format('', width=6*width))
        print('{:{width}}{:>{width}}{:>{width}}{:>{width}}{:>{width}}{:>{width}}'.format(
            'Node', 'SoC in', 'SoC out', 'Charge Time', 'Charger', 'Plug Type', width=width))
        print('{:{width}}{:>{width}}{:>{width}}{:>{width}}{:>{width}}'.format(
                '', '[%]', '[%]', '[min]', '[kW]', width=width))
        print('{:-^{width}}'.format('', width=6*width))
        
        print('{:<{width}}{:>{width}}{:>{width}}{:>{width}}{:>{width}}{:>{width}}'.format(
                'Start','',route_data['params']['soc_start'],'','','',width=width))

        for stop in solution['stops']:
            print(
                '{:<{width}}{:>{width}}{:>{width}}{:>{width}}{:>{width}}{:>{width}}'.format(
                    stop['stationId'],
                    str(stop['batteryArrival']),
                    str(stop['batteryDeparture']),
                    str(stop['chargetime']),
                    str(round(stop['plugKw'], 2)),
                    str(stop['plugType']),
                    width=width))

        print('{:<{width}}{:>{width}}{:>{width}}{:>{width}}{:>{width}}{:>{width}}'.format('Destination',solution['batteryArrival'],'','','','',width=width))
        print('{:=^{width}}'.format('', width=6*width))

        total_distance = sum([route_data['arcs'][arc]['distance'] for arc in active_arcs])/1000
        drive_time = sum([route_data['arcs'][arc]['time'] for arc in active_arcs])/60 + 5*(len(active_arcs)-1)
        charge_time = sum([stop['chargetime'] for stop in solution['stops']])
        total_route_time = drive_time + charge_time
        
        results = {'total_distance':total_distance,
                   'drive_time':drive_time,
                   'charge_time':charge_time,
                   'total_time':total_route_time,
                   }
        
        drive_time = divmod(drive_time, 60)
        charge_time = divmod(charge_time, 60)
        total_route_time = divmod(total_route_time, 60)

        print('Total Distance traveled: {:.1f} km'.format(total_distance))
        print(f'Drive time: {int(drive_time[0])} h {int(drive_time[1])} min')
        print(f'Charge time: {int(charge_time[0])} h {int(charge_time[1])} min')
        print(f'Total route time: {int(total_route_time[0])} h {int(total_route_time[1])} min')
        
        return results

def test_solve_route_problem(origin, destination, auto, solver, method):
    test_route = RouteRequest()
    test_route.soc_start = 20
    test_route.destination = destination
    test_route.start = origin
    test_route.plug_configuration_id = auto
    
    route_data = RouteService.calculate_route_data(test_route)

#    pprint.pprint(route_data)
#    return
    assert not route_data == {}
    
#    overheads = [n for n in range(5,105,5)]
#    gaps = np.linspace(0.05, 0.2, 7)
    final_route = RouteService.solve_route_problem(
        arcs=route_data['arcs'],
        nodes=route_data['nodes'],
        charge_curves=route_data['charge_curves'],
        params=route_data['params'],
        solver=solver,
        method=method
         )

    assert not final_route == {}

    summary, times = RouteUtil.print_solved_route_problem(route_data, final_route)
    print(summary)
    return final_route, times

def get_key(my_dict, val):
   for key, value in my_dict.items():
      if val == value:
         return key

def return_df(route, times):
    
    walltime = route['model'].WallTime()
    obj = route['model'].Objective().Value()
    lower_bound = route['model'].Objective().BestBound()
    gap = round((obj-lower_bound)/lower_bound,2)

    model_data = {'objective':obj,
                  'lower bound':lower_bound,
                  'gap':gap,
                  'wall time': walltime,
                  }
    
    return pd.concat([pd.DataFrame([model_data]), pd.DataFrame([times])], axis=1)

cities = {'Berlin':(13.41053,52.52437),
          'Dresden':(13.731751, 51.056044),
          'Hamburg':(10.01534, 53.57532),
          'Muenchen':(11.576124,48.137154),
          'Koeln':(6.953101,50.935173),
          }

#plug configurations for different EVs
autos = {'Model S 75D':452,
         'Model 3':437,
         'Ioniq Elektro':306,
         'Leaf 40kWh':354,
         'e-Golf':516,
         'Model S 60':441,
         'ID.3':542
         }


data = pd.DataFrame()
id_= 0
for ev in autos:
    for origin in cities:
        for destination in cities:
            if origin!=destination:
                for method in ["only_charge_time", "all"]:
                    for solver in ["CBC", "SCIP"]:
                        print("EV: {}, Route: {}-{}, Solver: {}, Method: {}, ID: {}".format(
                                ev, origin, destination, solver, method, id_
                                ))
                        try:
                            sol_route, times = test_solve_route_problem(origin=cities[origin],
                                                                        destination=cities[destination],
                                                                        auto=autos[ev],
                                                                        solver=solver,
                                                                        method=method)

                        except:
                            print('For auto {} with route {}-{} and solver {} there was a problem'.format(ev, origin, destination, solver))
                            continue
                        df = return_df(sol_route, times)
                        df['route'] = '{}-{}'.format(get_key(cities,cities[origin]), get_key(cities,cities[destination]))
                        df['auto'] = get_key(autos,autos[ev])
                        df['plug_configuration_id'] = autos[ev]
                        df['stops'] = len(sol_route['stops'])
                        df['solver'] = solver
                        df['method'] = method
                        df['Route_id'] = id_

                        data = data.append(df)
                        data.to_csv('scip_cbc_comp_10022021.csv', index=False)
                        id_=id_ + 1
