import logging
import os

from dotenv import load_dotenv

load_dotenv()
# OR, the same with increased verbosity
# load_dotenv(verbose=True)


class ConfigUtil:
    @staticmethod
    def get_env_variable(name, default=None) -> str:
        try:
            env_value = (
                os.getenv(name, default) if default is not None else os.getenv(name)
            )
            if env_value is None:
                raise KeyError
            else:
                return env_value
        except KeyError:
            message = (
                "ConfigUtil - get_env_variable() Expected env var '{}' not set.".format(
                    name
                )
            )
            raise Exception(message)


class Config(object):
    DEBUG = False
    TESTING = False

    # Postgres settings
    POSTGRES_HOST = ConfigUtil.get_env_variable("POSTGRES_HOST")
    POSTGRES_PORT = ConfigUtil.get_env_variable("POSTGRES_PORT")
    POSTGRES_URL = ConfigUtil.get_env_variable("POSTGRES_URL")
    POSTGRES_USER = ConfigUtil.get_env_variable("POSTGRES_USER")
    POSTGRES_PASSWORD = ConfigUtil.get_env_variable("POSTGRES_PASSWORD")
    POSTGRES_DB = ConfigUtil.get_env_variable("POSTGRES_DB")

    # MongoDB settings
    MONGODB_URI = ConfigUtil.get_env_variable("MONGODB_URI")
    MONGODB_DB = ConfigUtil.get_env_variable("MONGODB_DB")
    MONGODB_HISTORY_DATA = ConfigUtil.get_env_variable("MONGODB_HISTORY_DATA")


    # LOGGER settings
    LOGGER = logging.getLogger("eClever Route Calculation")
    LOGGER.setLevel(logging.DEBUG if os.getenv("DEBUG") == "true" else logging.INFO)
    # create file handler
    fh = logging.FileHandler(
        "eClever_route_calculation.log", mode="a", encoding="utf-8"
    )
    # create console handler
    ch = logging.StreamHandler()
    # create formatter and add it to the handlers
    formatter = logging.Formatter(
        "%(asctime)s - %(processName)s ( %(process)d ) - %(levelname)s - %(name)s - %(module)s "
        + "- %(message)s"
    )
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    LOGGER.addHandler(fh)
    LOGGER.addHandler(ch)

class DevelopmentConfig(Config):
    DEBUG = True


class TestConfig(Config):
    TESTING = True


class ProductionConfig(Config):
    pass


def get_config(env=None):
    if env is None:
        try:
            env = ConfigUtil.get_env_variable("ENV")
        except Exception:
            env = "development"
            print("'env' is not set, using env:", env)

    if env == "production":
        return ProductionConfig()
    elif env == "test":
        return TestConfig()

    return DevelopmentConfig()
