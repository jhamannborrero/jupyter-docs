from config import get_config

import datetime
import numpy as np
import pandas as pd
import geopy.distance


class DataCollectorHelper:
    """
    Class helper to perform various data manipulation
    """

    logger = get_config().LOGGER

    @staticmethod
    def sort_drives(dff):
        """
        Sort vehicle drive data.

        Arguments:
            df: Pandas DataFrame with the drive data.

        Returns: A list where each element is an AutoDrive object.
        """
        MIN_SPEED = 3  # km/h
        soc = dff["hvSocActualDisplay"]
        dff = dff.interpolate(limit_direction="forward")
        dff["hvSocActualDisplay"] = soc
        dff = dff.dropna(subset=["hvSocActualDisplay"])
        dff = dff[dff.speed > MIN_SPEED].dropna()

        # add timedeltas
        dff["timedelta"] = pd.Series(dff.index).diff(1).values

        if dff.empty:
            DataCollectorHelper.logger.debug(
                "DataCollectorHelper - sort_drives() no drives sorted!",
            )
            return

        # if time delta is larger than 5 mins, this will be considered another drive/charge
        peaks_idx = dff[dff.timedelta > datetime.timedelta(seconds=300)]
        # add initial index value and sort index
        peaks_idx = peaks_idx.append([dff.iloc[0]]).sort_index()
        peaks_idx = peaks_idx.index.values

        drives = [
            dff.loc[peaks_idx[i] : peaks_idx[i + 1]][:-1]  # noqa: E203
            for i in range(len(peaks_idx[:-1]))
        ]

        del dff

        drives = [drive for drive in drives if len(drive.hvSocActualDisplay) >= 10]

        return drives

    @staticmethod
    def get_geo_distance(lat, long):
        """
        Calculates the distance between two points based on its latitude and
        longitude.

        Arguments:
            line : Route Profile (pandas DataFrame) with lat and long info

        Returns:
            two lists, one with the distance between each point and the other
            with thecumulative sum of the distances.
        """

        x0 = list(zip(lat, long))
        s = [0]
        for start, destination in zip(x0, x0[1:]):
            s.append(geopy.distance.distance(start, destination).km)

        return s, np.cumsum(s)
