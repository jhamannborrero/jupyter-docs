from datetime import datetime
from pymongo import MongoClient, CursorType
from config import get_config

import pandas as pd
import psycopg2
import numpy as np
import re


class DatabaseHelper:
    """
    Class helper to perform various database queries.
    """

    logger = get_config().LOGGER

    # Number of documents to retrieve per batch in a mongodb query
    MONGO_BATCH_SIZE = 10000

    COL_NAMES_REGEX = r"(?<=\.)\w+"

    @classmethod
    def connect_mongodb(cls):
        DatabaseHelper.logger.debug("DatabaseHelper - connect_mongodb() ")

        mongodb_connection = MongoClient(
            get_config().MONGODB_URI,
        )

        return mongodb_connection

    @classmethod
    def connect_pg_db(cls):
        DatabaseHelper.logger.debug(
            "DatabaseHelper - connect_db() db: {}".format(
                get_config().POSTGRES_DB,
            )
        )

        db_connection = psycopg2.connect(
            database=get_config().POSTGRES_DB,
            user=get_config().POSTGRES_USER,
            password=get_config().POSTGRES_PASSWORD,
            host=get_config().POSTGRES_HOST,
            port=get_config().POSTGRES_PORT,
        )

        db_connection.set_session(readonly=True, autocommit=True)

        return db_connection

    @staticmethod
    def query_active_users(pg_connection):
        """
        Query the users that are active.

        Returns:
            A pandas DataFrame with user ID, vehicle ID, vehicle type, type internal ID,
            vin and data validity range (time)
        """
        query = (
            'SELECT uv.user_id, uv.vehicle_id, v.vehiclevariant_id AS "vehicleVariantId", '
            'vv.consumptioncurvegroup_id AS "consumptionCurveGroupId", '
            'ac.obd_id, uv."validFrom", uv."validTo" '
            "FROM users_vehicles AS uv "
            "JOIN vehicle AS v ON v.id = uv.vehicle_id "
            "JOIN vehiclevariants AS vv ON v.vehiclevariant_id = vv.id "
            "JOIN account AS ac ON ac.id = v.account_id "
            'WHERE (uv."validFrom" <= NOW() AND '
            '(uv."validTo" IS NULL OR uv."validTo" >= NOW())) '
        )

        active_users = pd.read_sql_query(query, pg_connection)

        return active_users.fillna(np.nan).replace([np.nan], [None])

    @staticmethod
    def query_history_data(mongo_connection=None):
        """
        Query data for vehicle with given OBD ID.

        Parameters:
            vehicle_id (string) : Vehicle id.
            valid_from (timestamp): Date from which the data is valid.
            valid_to (timestamp): Date until which the data is valid.
            is_obd (boolean): if data comes from OBD the set to True
            batches (boolean): set to True if analysis will be done on batches
            mongo_connection (object): mongodb connection object.

        Returns:
            If batches is False a pandas DataFrame with available vehicle data in the estipulated
            time window.
            If batches is True a mongoDB query cursor object
        """
        # query = [
        #     {
        #         "$match": {
        #             "$and": [
        #                 {"vehicleId": vehicle_id},  # NOSONAR  # NOSONAR
        #                 {
        #                     "cPack.timestamp": {"$gte": from_date, "$lt": to_date}
        #                 },  # NOSONAR
        #                 {
        #                     "$or": [
        #                         {"eMotor.hvSocActualDisplay": {"$exists": "True"}},
        #                         {"eMotor.hvSocActualBms": {"$exists": "True"}},
        #                     ]
        #                 },
        #             ]
        #         },
        #     },
        #     {
        #         "$project": {
        #             "eMotor.hvSocActualDisplay": 1,  # NOSONAR
        #             "eMotor.hvSocActualBms": 1,  # NOSONAR
        #             "eMotor.hvBatteryVoltage": 1,  # NOSONAR
        #             "eMotor.hvBatteryCurrent": 1,  # NOSONAR
        #             "eMotor.hvBatteryTemp": 1,  # NOSONAR
        #             "basicCarInfo.odoMeter": 1,  # NOSONAR
        #             "climate.insideTempSetting": 1,  # NOSONAR
        #             "climate.insideActualTemp": 1,  # NOSONAR
        #             "climate.outsideAirTemp": 1,  # NOSONAR
        #             "driveState.speed": 1,  # NOSONAR
        #             "driveState.latitude": 1,  # NOSONAR
        #             "driveState.longitude": 1,  # NOSONAR
        #             "driveState.altitude": 1,  # NOSONAR
        #             "min": {"$min": "$driveState.speed"},
        #             "_id": 0,
        #         },
        #     },
        #     {
        #         "$match": {
        #             "min.0": {"$gt": 3},
        #         },
        #     },
        #     {
        #         "$project": {
        #             "min": 0,
        #         },
        #     },
        # ]
        

        collection = mongo_connection[get_config().MONGODB_DB][
            get_config().MONGODB_HISTORY_DATA
        ]

        return pd.DataFrame(list(collection.find()))


    @staticmethod
    def query_errors(mongo_connection=None):
        """
        Query data for vehicle with given OBD ID.

        Parameters:
            vehicle_id (string) : Vehicle id.
            valid_from (timestamp): Date from which the data is valid.
            valid_to (timestamp): Date until which the data is valid.
            is_obd (boolean): if data comes from OBD the set to True
            batches (boolean): set to True if analysis will be done on batches
            mongo_connection (object): mongodb connection object.

        Returns:
            If batches is False a pandas DataFrame with available vehicle data in the estipulated
            time window.
            If batches is True a mongoDB query cursor object
        """
        # query = [
        #     {
        #         "$match": {
        #             "$and": [
        #                 {"vehicleId": vehicle_id},  # NOSONAR  # NOSONAR
        #                 {
        #                     "cPack.timestamp": {"$gte": from_date, "$lt": to_date}
        #                 },  # NOSONAR
        #                 {
        #                     "$or": [
        #                         {"eMotor.hvSocActualDisplay": {"$exists": "True"}},
        #                         {"eMotor.hvSocActualBms": {"$exists": "True"}},
        #                     ]
        #                 },
        #             ]
        #         },
        #     },
        #     {
        #         "$project": {
        #             "eMotor.hvSocActualDisplay": 1,  # NOSONAR
        #             "eMotor.hvSocActualBms": 1,  # NOSONAR
        #             "eMotor.hvBatteryVoltage": 1,  # NOSONAR
        #             "eMotor.hvBatteryCurrent": 1,  # NOSONAR
        #             "eMotor.hvBatteryTemp": 1,  # NOSONAR
        #             "basicCarInfo.odoMeter": 1,  # NOSONAR
        #             "climate.insideTempSetting": 1,  # NOSONAR
        #             "climate.insideActualTemp": 1,  # NOSONAR
        #             "climate.outsideAirTemp": 1,  # NOSONAR
        #             "driveState.speed": 1,  # NOSONAR
        #             "driveState.latitude": 1,  # NOSONAR
        #             "driveState.longitude": 1,  # NOSONAR
        #             "driveState.altitude": 1,  # NOSONAR
        #             "min": {"$min": "$driveState.speed"},
        #             "_id": 0,
        #         },
        #     },
        #     {
        #         "$match": {
        #             "min.0": {"$gt": 3},
        #         },
        #     },
        #     {
        #         "$project": {
        #             "min": 0,
        #         },
        #     },
        # ]
        collection = mongo_connection[get_config().MONGODB_DB][
            get_config().MONGODB_HISTORY_DATA
        ]

        return pd.DataFrame(list(collection.find().limit(500)))


    @staticmethod
    def mongo_to_pandas_batches(iterator, batch_size: int):
        """Turn an mongoDB cursor iterator into multiple small pandas.DataFrame

        Parameters:
            iterator (object) : mongoDB cursor object.
            batch_size (int) : Number of documents per batch.

        Returns:
            DataFrame with data.

        This is a balance between memory and efficiency
        """
        records = []
        frames = []
        n = 0
        for i, record in enumerate(iterator):
            records.append(record)
            if i % batch_size == batch_size - 1:
                frames.append(pd.json_normalize(records))
                records = []
                n += 1
                DatabaseHelper.logger.debug(
                    "DatabaseHelper - mongo_to_pandas_batches() %s chunks processed", n
                )

        if records:
            frames.append(pd.json_normalize(records))

            DatabaseHelper.logger.debug(
                "DatabaseHelper - mongo_to_pandas_batches() final chunk processed",
            )

        if frames:
            return pd.concat(frames)

    @staticmethod
    def unpack_col(col, col_name):
        """
        Unpacks data columns in OBD data

        Parameters:
            col: pandas Series with the column packed data
            col_name : label for the output column.

        Returns:
            pandas DataFrame with unpacked data as columns. The DataFrame index
            is the unix time at which the data was collected.
        """
        col = col.copy().dropna()
        col = col.map(lambda x: np.array(x))
        values = np.concatenate(
            col.map(lambda x: x[[n for n in range(len(x))], [0]]).values
        )
        timestamp = np.concatenate(
            col.map(lambda x: x[[n for n in range(len(x))], [1]]).values
        )
        unpacked_col = pd.DataFrame({"timestamp": timestamp, col_name: values})
        unpacked_col = unpacked_col.drop_duplicates(subset="timestamp")
        return unpacked_col.set_index("timestamp")

    @staticmethod
    def unpack_data(df):
        """
        Unpacks OBD raw data comming from MongoDB query

        Parameters:
            def : pandas DataFrame with the packed data

        Returns:
            pandas DataFrame with unpacked data as columns. The DataFrame index
            is the timestamp.
        """
        dff = df.reset_index(drop=True)

        unpacked = pd.DataFrame()

        for col in dff.columns:
            unpacked_col = DatabaseHelper.unpack_col(
                dff[col], re.findall(DatabaseHelper.COL_NAMES_REGEX, col)[0]
            )

            unpacked = unpacked.join(unpacked_col, how="outer")

            dff = dff.drop(columns=[col])

        unpacked.index = pd.to_datetime(unpacked.index, unit="s")

        return unpacked
