import random as rn
import pandas as pd
import sys
sys.path.append('/home/jehamann/eClever/repositories/route-calculation/src/')

import os
import json
import random
import re
from services import RouteService
from helpers import DatabaseHelper
from models import  RouteRequest, VehicleVariantId

with open("german_cities.json", "r") as f:
    cities = json.load(f)

#plug configurations for different EVs
autos = {'Model S 75D':452,
         'Model 3':437,
         'Ioniq Elektro':306,
         'Leaf 40kWh':354,
         'e-Golf':516,
         'Model S 60':441,
         'ID.3':542
         }

def create_url_list():
    for ev in autos:
        for origin in cities:
            for destination in cities:
                if origin != destination:
                    with open('test_urls.txt', 'a') as f:
                        f.write("127.0.0.1:5000/v1/route-calculation?destinationLocationLat={}"\
                                "&destinationLocationLng={}&plugConfigurationId={}&socStart={}"\
                                "&startLocationLat={}&startLocationLng={}\n".format(
                                        cities[destination][1],
                                        cities[destination][0],
                                        autos[ev],
                                        rn.randrange(5,100),
                                        cities[origin][1],
                                        cities[origin][0],
                                        ))


def create_urls_matrix_osrm():
    """
    Create url files with test urls to use with siege for matrix requests using osrm.
    
    """
    autos_ids = [i.value for i in list(VehicleVariantId)]
    base_url = "https://osrm.routing.eclever.net/table/v1/driving/"
    
    for n in range(200):
        route_request = RouteRequest()
        route_request.soc_start= 90
        origin, destination = random.sample(cities, 2)
        route_request.start = (float(origin["lng"]), float(origin["lat"])) 
        route_request.destination = (float(destination["lng"]), float(destination["lat"]))
        route_request.vehicle_variant_id = random.sample(autos_ids, 1)[0]
    
        with DatabaseHelper.connect_mongodb() as mongo_connection:
            pg_connection_pool = DatabaseHelper.connect_db_pool()
        
            try:
                (
                    vehicle_type_df,
                    plugs_df,
                    vehicle_connectors,
                    tariffs
                ) = RouteService.collect_auto_user_data(
                    route_request, pg_connection_pool, mongo_connection
                )
        
                route_data = RouteService.calculate_route_data(
                route_request,
                vehicle_type_df,
                plugs_df,
                vehicle_connectors,
                preference="fastest",
                pg_connection_pool=pg_connection_pool,
                mongo_connection=mongo_connection,
                )["nodes"]
                
                route_data = [route_data[i]["location"] for i in route_data]
                route_data = ";".join([f"{i[0]},{i[1]}" for i in route_data])
    
                with open('test_urls_matrix_osrm.txt', 'a') as f:
                    f.write(
                        base_url + route_data
                        + "?annotations=duration,distance&generate_hints=false\n"
                        )
            except:
                pass
      
            finally:
                mongo_connection.close()
                pg_connection_pool.closeall()
                
def create_urls_matrix_valhalla():
    """
    Create url files with test urls to use with siege for matrix requests using valhalla.
    For this a previous file needs to be created using the create_urls_matrix_osrm() function. The
    data in this file is used to create the matrix url file for valhalla.
    """
    base_url = "https://valhalla.routing.eclever.net/sources_to_targets"
    with open('test_urls_matrix_osrm.txt', 'r') as fi, open('test_urls_matrix_valhalla.txt', 'a') as fo:
        for line in fi:
            locations = re.findall("(\d+.\d+),(\d+.\d+)",line)
            locations = [
                {
                    "lon": location[0],
                    "lat": location[1],
                    }
                for location in locations
                ]

            payload = {
                "sources": locations,
                "targets": locations,
                "costing":"auto"
                }

            payload = json.dumps(payload)

            fo.write(
                f"{base_url} POST {payload}\n"
                )

def create_url_list_routing_apis(api):
    """
    Create url files with test urls to use with siege.
    
    Parameters:
        api (str): eather "osrm" or "valhalla" to select the requests syntax for the urls
    """
    if api == "osrm":
        base_url = "https://osrm.routing.eclever.net/route/v1/driving/"
        
        for n in range(500):
            origin, destination = random.sample(cities, 2)
            with open('test_urls_osrm.txt', 'a') as f:
                f.write(
                    base_url + f"{origin['lng']},{origin['lat']}"
                    + f";{destination['lng']},{destination['lat']}"
                    + "?overview=full&geometries=geojson\n"
                    )
    if api == "valhalla":
        base_url = "https://valhalla.routing.eclever.net/route?json="
        
        for n in range(500):
            origin, destination = random.sample(cities, 2)
            payload = {
                "locations": [
                    {
                        "lon": origin["lng"],
                        "lat": origin["lat"]
                        },
                    {
                        "lon": destination["lng"],
                        "lat": destination["lat"]
                        },
                    ],
                "costing": "auto",
                }

            payload = json.dumps(payload)
            with open('test_urls_valhalla.txt', 'a') as f:
                f.write(
                    base_url + payload + "\n"
                    )
        
create_url_list_routing_apis("valhalla")

def set_gunicorn_file(workers, threads):
    head = "import os\n"\
    "from distutils.util import strtobool\n\n"\
    'bind = "0.0.0.0:" + os.getenv("FLASK_RUN_PORT", "5000")\n'\
    'accesslog = "-"\n'\
    "access_log_format = \"%(h)s %(l)s %(u)s %(t)s '%(r)s' %(s)s %(b)s '%(f)s' '%(a)s' in %(L)ss\"\n"\
    'errorlog = "-"\n'\
    'loglevel = os.getenv("GUNICORN_LOG_LEVEL", "debug")\n'\
    'worker_class = os.getenv("GUNICORN_WORKER_CLASS", "gthread")\n'\
    'reload = bool(strtobool(os.getenv("GUNICORN_RELOAD", "true")))\n'\
    'timeout = 60\n'\
    "workers = {}\n"\
    'threads = int(os.getenv("GUNICORN_THREADS", {}))'.format(workers, threads)

    with open('gunicorn_test.py', 'w') as f:
        f.write(head)

def set_siege_rampup(workers, threads, concurrent, session_time):
    return 'siege --concurrent={2} --time={3}M -d1 --header "Origin: localhost" -f '\
    'test_urls.txt --internet > test_rampup_{0}w_{1}th_{2}c.txt'.format(
            workers, threads,int(concurrent), int(session_time))

def set_siege_rampup_routing_apis(concurrent, session_time, url_file, filename):
    """
    Creates a string with the siege command to perform a load test. Stores the results of the test
    in a file located in the filename folder. 
    
    Parameters:
        concurrent (int): Number of concurrent requests
        session_time (int): Durantion of the test in seconds
        url_file (str): name of the file with the urls to use for the test
        filename (str): Folder where to save the test files
    """
    return f'siege --concurrent={concurrent} --time={session_time}S -d1 -f '\
    f'{url_file} --internet -v > {filename}/test_rampup_{concurrent}c.txt'

def set_gunicorn():
    return '/home/jehamann/eClever/repositories/tests/siege/venv/bin/gunicorn '\
            '--chdir /home/jehamann/eClever/repositories/route-calculation/src/ '\
            '-c gunicorn_test.py main:app &'


def read_session_res(file_name):
    with open(file_name, 'r') as f:
        timestamp =[]
        resp = []
        time = []
        for line in f:
            entry = line.split()
            timestamp.append('{} {}'.format(entry[1], entry[2][:-1]))
            resp.append(int(entry[4]))
            time.append(float(entry[5]))

    return pd.DataFrame({'timestamp':timestamp, 'response':resp, 'time':time})

def read_logs():
    """
    Reads log date from siege and returns a dataframe
    """
    file = '/home/jehamann/.siege/siege.log'
    data = pd.read_csv(file)
    # clean columns string format
    data.columns = [col.strip() for col in data.columns]
    
    return data

def ramp_up_test(steps, ramp_time, url_file=None, filename="test"):
    """
    Performs a ramp-up test where the amount of parallel users are increased to a maximum of 300 
    in a number of steps during the requested ramp_time. The data will be stored in the 
    filename folder.
    
    Parameters:
        steps (int): number of steps to increment users
        ramp_time (int): time interval in which the users are increased until the maximum 
        number of users in seconds
        url_file (str): name of the file with the urls to use for the test
        filename (str): name of the folder where data will be stored
        
    Returns:
        pandas DataFrame with results of the test.
    """
    os.system(f"mkdir {filename}")
    max_users = 300
    step_time = ramp_time/steps
    user_step = max_users//steps 
    concurrent_list = [(i+1)*user_step for i in range(steps)]
    # ramp up
    for concur in concurrent_list:
        cmd = set_siege_rampup_routing_apis(concur, step_time, url_file, filename)
        print(cmd)
        os.system(cmd)
        
    # collect data from logs
    data = read_logs()
    data["Time"] = data["Elap Time"].cumsum()
    data['Availability'] = data['OKAY'] * 100 / (data['OKAY'] + data['Failed'])
    data["users"] = concurrent_list
    
    data.to_csv(f"{filename}/data.csv", index=False)
    os.system("rm /home/jehamann/.siege/siege.log")
    return data
    

# data = ramp_up_test(20, 3600,"test_urls_valhalla.txt", "valhalla_a_to_b_load_test")
# data = ramp_up_test(20, 3600,"test_urls_matrix.txt", "osrm_matrix_load_test")
data = ramp_up_test(20, 3600,"test_urls.txt", "osrm_a_to_b_load_test")
# data = ramp_up_test(20, 3600,"test_urls_matrix_valhalla.txt", "valhalla_matrix_load_test")



#params = [(100.0, 10), (50.0, 20), (25.0, 40), (20.0, 50), (10.0, 100), (5.0, 200), (4.0, 250), 
#          (2.0, 500), (1.0, 1000)]

#params = [(10.0, 100), (5.0, 200), (2.0, 500), (1.0, 1000)]
#params = [(1, (1+n)*50) for n in range(2)]
    

# params = [(15.0, 20), (5.0, 60), (3.0, 100), (1.0, 300)]
# cores = [n*2+1 for n in range(0,9,2)]

#for worker in cores:
#    threads = 8
#    set_gunicorn_file(worker, threads)
#    os.system(set_gunicorn())
#    time.sleep(10)
#    for reps, concur in params:
#        os.system(set_siege(worker, threads, concur, reps))
#
#    os.system('killall -r -9 gunicorn')


#for reps, concur in params[3:]:
#    worker = 5
#    threads = 1
#    os.system(set_siege(worker, threads, concur, reps))

#os.system('killall -r -9 gunicorn')


# def ramp_up_test(worker, threads, session_time):
#     for concur in [10*(n+1) for n in range(15)]:
# #        print(set_siege_rampup(worker, threads, concur, session_time))
#         os.system(set_siege_rampup(worker, threads, concur, session_time))

#ramp_up_test(1, 8, 10)



