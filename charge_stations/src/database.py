import psycopg2
import numpy as np
import pandas as pd
import math
import re
import pickle

from pymongo import MongoClient
from datetime import datetime, timedelta
from config import get_config

class DatabaseHelper:

    @classmethod
    def connect_db(cls):
        db_connection = psycopg2.connect(
            database=get_config().POSTGRES_DB,
            user=get_config().POSTGRES_USER,
            password=get_config().POSTGRES_PASSWORD,
            host=get_config().POSTGRES_HOST,
            port=get_config().POSTGRES_PORT,
            connect_timeout=get_config().TIMEOUT,
        )

        db_connection.set_session(readonly=True, autocommit=True)

        return db_connection

    @classmethod
    def connect_mongodb(cls):

        mongodb_connection = MongoClient(
            get_config().MONGODB_URI,
        )

        return mongodb_connection

    @staticmethod
    def get_chargepoint_actual_status(chargepoints):
        """
        Docu
        """
        
        query = {"plugId": {"$in": chargepoints}}
        fields = {"__v":0,"_id":0}

        with DatabaseHelper.connect_mongodb() as conn:
            collection = conn[get_config().MONGODB_DB][get_config().MONGODB_CP_LIVE_STATUS_COLLECTION]

            data = pd.DataFrame(list(collection.find(query, fields)))

        return data

    @staticmethod
    def get_chargepoint_history(days):
        """
        Docu
        """
        from_date = datetime.today() - timedelta(days=days)
        
        eclever_evse_ids = DatabaseHelper.get_eclever_chargepoints()
        
        query = {
            "lastUpdate": {"$gte": from_date},
            "plugId":{"$in":eclever_evse_ids},
            "status":{"$in":['AVAILABLE', 'UNAVAILABLE']},
        }
        fields = {"plugId":1, "lastUpdate":1, "status":1, "_id":0}
    
        with DatabaseHelper.connect_mongodb() as conn:
            collection = conn[get_config().MONGODB_DB][get_config().MONGODB_CP_HISTORY_COLLECTION]
    
            data = pd.DataFrame(list(collection.find(query, fields)))
        
        return data
      

    @staticmethod
    def query_chargepoint(chargepoint_ids):
        """
        """
        query = (
            'SELECT * FROM station."routeCalculationView" '
            f"WHERE chargepoint_id IN ({chargepoint_ids})"
        )
        with DatabaseHelper.connect_db() as conn:
            
            chargepoint_df = pd.read_sql_query(query, conn)

        return chargepoint_df


    @staticmethod
    def get_eclever_chargepoints():
        """
        """
        query = 'SELECT "evseId" FROM station.chargepoints where "evseId" != \'\''
        with DatabaseHelper.connect_db() as conn:
    
            chargepoint_df = pd.read_sql_query(query, conn)
    
        return chargepoint_df.values.flatten().tolist()


    @staticmethod
    def get_chargepoints():
        """
        """
        query = 'SELECT "evseId", "plugType", chargegroup_id FROM station.chargepoints'
        with DatabaseHelper.connect_db() as conn:
    
            chargepoint_df = pd.read_sql_query(query, conn)
    
        return chargepoint_df


    @staticmethod
    def query_chargegroups(chargegroup_ids=None):
        # ids = ",".join(chargegroup_ids.astype(str))
        query = (
            'WITH grouped AS ('
            'SELECT chargegroup_id, "plugType", COUNT(id) AS plug_count, AVG("chargingPower") AS mean_power, ARRAY_AGG("evseId") AS evse_ids '
            "FROM station.chargepoints "
            # f"WHERE chargegroup_id IN ({ids}) "
            'GROUP BY chargegroup_id, "plugType"'
            ')'
            'SELECT grouped.chargegroup_id, "plugType", plug_count, total_plugs, foo.mean_power::int, evse_ids '
            "FROM grouped "
            "LEFT JOIN ("
            "SELECT grouped.chargegroup_id, SUM(plug_count) AS total_plugs, AVG(mean_power) AS mean_power "
            "FROM grouped "
            "GROUP BY chargegroup_id"
            ") AS foo ON foo.chargegroup_id = grouped.chargegroup_id "
            "ORDER BY chargegroup_id"
        )
    
        with DatabaseHelper.connect_db() as conn:
            df = pd.read_sql_query(query, conn)
        return df    


    @staticmethod
    def get_cg_chargepoint_history(evse_ids, days):
        """
        Docu
        """
        from_date = datetime.today() - timedelta(days=days)
       
        query = {
            "plugId":{"$in":evse_ids},
            "lastUpdate": {"$gte": from_date},
            # "status":{"$in":['AVAILABLE', 'UNAVAILABLE']},
        }
        fields = {"lastUpdate":1, "plugId":1, "status":1, "_id":0}
    
        with DatabaseHelper.connect_mongodb() as conn:
            collection = conn[get_config().MONGODB_DB][get_config().MONGODB_CP_HISTORY_COLLECTION]
    
            data = pd.DataFrame(list(collection.find(query, fields)))
        
        return data


    @staticmethod
    def aggregate(df, cg_df):
        df = df.set_index("lastUpdate")
        cps = df.evseId.unique()
        l = []
        for cp in cps:
            for plug in df.query(f"evseId == '{cp}'").plugType.unique():
                _ = df.query(f"evseId == '{cp}' and plugType == '{plug}'").resample("H").ffill().dropna()
                l.append(_)
                         
        df_agg = pd.concat(l)
        df_agg["status"] = df_agg["status"].apply(lambda x: 1 if x == "AVAILABLE" else 0)
        df_agg = df_agg.reset_index()
        cg_id = df_agg.chargegroup_id.iloc[0]
        df_agg =df_agg[["status","lastUpdate", "plugType"]].groupby(["lastUpdate", "plugType"]).sum(numeric_only=True)
        df_agg = (
            df_agg.reset_index()
            .merge(cg_df.query(f"chargegroup_id == {cg_id}"), on="plugType")
            # .reset_index()
        )
        
        # df_agg = get_time_features(df_agg)
        # df_agg = (
        #     df_agg.set_index("lastUpdate")
        #     .sort_index()
        # )
        df_agg["availability"] = df_agg["status"].div(df_agg["plug_count"])
        return df_agg


    @staticmethod
    def test(n):
        """
        Docu
        """
        cg_df = DatabaseHelper.query_chargegroups()
        cg_ids = cg_df.chargegroup_id.unique() 
        cp_df = DatabaseHelper.get_chargepoints()
        id_ = cg_ids[n]
        evse_ids = cg_df.query(f"chargegroup_id == {id_}").evse_ids.sum()
        data = DatabaseHelper.get_cg_chargepoint_history(evse_ids, 365)
        data = (
            data.merge(cp_df, left_on="plugId", right_on="evseId", how="inner")
            .drop("plugId", axis=1)
            )
        
        data = DatabaseHelper.aggregate(data, cg_df)
        return data

if __name__ == "__main__":
    data = DatabaseHelper.test(4)
    print(f"data from {data.lastUpdate.min()} to {data.lastUpdate.max()}")