import os

from dotenv import load_dotenv

load_dotenv()

# OR, the same with increased verbosity
# load_dotenv(verbose=True)


class ConfigUtil:
    @staticmethod
    def get_env_variable(name, default=None) -> str:
        try:
            env_value = (
                os.getenv(name, default) if default is not None else os.getenv(name)
            )
            if env_value is None:
                raise KeyError
            else:
                return env_value
        except KeyError:
            message = (
                "ConfigUtil - get_env_variable() Expected env var '{}' not set.".format(
                    name
                )
            )
            raise Exception(message)

class Config(object):
    DEBUG = False
    TESTING = False

    # Postgres settings
    POSTGRES_HOST = ConfigUtil.get_env_variable("POSTGRES_HOST")
    POSTGRES_PORT = ConfigUtil.get_env_variable("POSTGRES_PORT")
    POSTGRES_URL = ConfigUtil.get_env_variable("POSTGRES_URL")
    POSTGRES_USER = ConfigUtil.get_env_variable("POSTGRES_USER")
    POSTGRES_PASSWORD = ConfigUtil.get_env_variable("POSTGRES_PASSWORD")
    POSTGRES_DB = ConfigUtil.get_env_variable("POSTGRES_DB")

    # MongoDB settings
    MONGODB_URI = ConfigUtil.get_env_variable("MONGODB_URI")
    MONGODB_DB = ConfigUtil.get_env_variable("MONGODB_DB")
    MONGODB_CP_LIVE_STATUS_COLLECTION = ConfigUtil.get_env_variable("MONGODB_CP_LIVE_STATUS_COLLECTION")
    MONGODB_CP_HISTORY_COLLECTION = ConfigUtil.get_env_variable("MONGODB_CP_HISTORY_COLLECTION")
    
    API_RUN_PORT = ConfigUtil.get_env_variable("API_RUN_PORT", "7000")
    CONTAINER_EXPOSE_HOST = ConfigUtil.get_env_variable("CONTAINER_EXPOSE_HOST", "127.0.0.1")
    CONTAINER_EXPOSE_PORT = ConfigUtil.get_env_variable("CONTAINER_EXPOSE_PORT", "8787")

    GENERATIONS = int(ConfigUtil.get_env_variable("GENERATIONS", "3"))
    POPULATION = int(ConfigUtil.get_env_variable("POPULATION", "100"))
    DASK = (
        True if ConfigUtil.get_env_variable("DASK") == "true" else False
        )

    ENV = ConfigUtil.get_env_variable("ENV")
    TIMEOUT = ConfigUtil.get_env_variable("TIMEOUT")


class DevelopmentConfig(Config):
    DEBUG = True


class TestConfig(Config):
    TESTING = True


class ProductionConfig(Config):
    pass


def get_config(env=None):
    if env is None:
        try:
            env = ConfigUtil.get_env_variable("ENV")
        except Exception:
            env = "development"
            print("'env' is not set, using env:", env)

    if env == "production":
        return ProductionConfig()
    elif env == "test":
        return TestConfig()

    return DevelopmentConfig()
